const loadAudio = (element, iconSrc) => {
    for (let idx = 0; idx < element.length; idx++) {
        const audio = element[idx].querySelector(".audio");
        const soundwave = element[idx].querySelector(".audio-soundwave");
        const slider = element[idx].querySelector(".audio-slider");
        const playBtn = element[idx].querySelector(".play-button");
        const muteBtn = element[idx].querySelector(".mute-button");
        let audioTotalTime = element[idx].querySelector(".audio-total-time");
        let audioCurrTime = element[idx].querySelector(".audio-current-time");

        const rangeSlider = () => {
            let position = 0;

            if (!isNaN(audio.duration)) {
                position = audio.currentTime * (100 / audio.duration);
                slider.value = position;
            }

            if (audio.ended) {
                playBtn.querySelector("img").setAttribute("src", `${iconSrc}play.svg`);
                soundwave.classList.remove('play');
            }
        }

        const getTotalDuration = () => {
            let audioDuration = audio.duration;
            let totalMin = Math.floor(audioDuration / 60);
            let totalSec = Math.floor(audioDuration % 60);
            
            if (totalSec < 10) totalSec = `0${totalSec}`;
            
            audioTotalTime.innerText = `${totalMin}:${totalSec}`
        }

        // Play & Pause button
        playBtn.addEventListener("click", (e) => {
            if (audio.paused) {
                audio.play();
                getTotalDuration();
                soundwave.classList.add('play');
                timer = setInterval(rangeSlider, 100);
                e.currentTarget.querySelector("img").setAttribute("src", `${iconSrc}pause.svg`);
            } else {
                audio.pause();
                soundwave.classList.remove('play');
                e.currentTarget.querySelector("img").setAttribute("src", `${iconSrc}play.svg`);
            }
        });

        // Mute button
        muteBtn.addEventListener("click", (e) => {
            if (audio.muted) {
                audio.muted = !audio.muted;
                e.currentTarget.querySelector("img").setAttribute("src", `${iconSrc}volume-up.svg`);
            } else {
                audio.muted = !audio.muted;
                e.currentTarget.querySelector("img").setAttribute("src", `${iconSrc}volume-mute.svg`);
            }
        });

        // Change Duration
        slider.addEventListener('input', (e) => {
            sliderPosition = audio.duration * (slider.value / 100);
            audio.currentTime = sliderPosition;
        }, false);

        audio.addEventListener('timeupdate', (e) => {
            const currTime = e.target.currentTime;
            
            
            let currMin = Math.floor(currTime / 60);
            let currSec = Math.floor(currTime % 60);
            
            if (currSec < 10) currSec = `0${currSec}`;
            
            audioCurrTime.innerText = `${currMin}:${currSec}`
        });

        getTotalDuration();
    }
};